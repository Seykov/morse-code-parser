#ifndef MORSECODEPARSER_H_INCLUDED
#define MORSECODEPARSER_H_INCLUDED
#include <iostream>
#include <string>
#include <map>
#include <set>
#include "Alphabet.h"

using namespace std;

class MorseCodeParser {
public:
    MorseCodeParser();

    string parseMorseSequence(string _sequence);
    void insertNewSymbol(char _symbol, string _morse);
    void displayAllSymbols();


private:
    map<string, char> morseToEnglish;
    set<char> allowedMorseSymbols;
    char getSymbol(string _morse);
    void printMorsePair(map<string, char>::iterator iter);
    bool validateMorseSequence(string _sequence);
    bool validateInsertionOfNewSymbol(char _symbol, string _morse);

};

#endif // MORSECODEPARSER_H_INCLUDED
