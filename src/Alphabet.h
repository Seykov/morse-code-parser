#ifndef ALPHABET_H_INCLUDED
#define ALPHABET_H_INCLUDED
#include <iostream>
#include <map>
#include <string>
#include <set>

using namespace std;

class Alphabet {
public:
    Alphabet();
    map<string, char> getEnglishAlphabetAndNumbers()const;
    set<char> getAllowedMorseSymbols()const;

private:
    map<string, char> morseToEnglish;
    set<char> allowedSymbols;
    void createEnglishAlphabetWithNumbers();
    void defineAllowedMorseSymbols();
};


#endif // ALPHABET_H_INCLUDED
