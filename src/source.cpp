#include <iostream>
#include <string>
#include "Alphabet.h"
#include "MorseCodeParser.h"

using namespace std;

int main() {

    MorseCodeParser parser;
    parser.insertNewSymbol('!', "------");

    string seq = ".... . -.--   .--- ..- -.. . ------";

    cout << a.parseMorseSequence(seq) << endl;

    /*
    a.getSymbol(".");
    a.getSymbol(".-");
    a.getSymbol("-.");
    a.getSymbol("...");
    a.getSymbol("-.-");
    a.getSymbol(".......");

    a.insertNewSymbol('a', "----------");
    a.insertNewSymbol('%', ".-");

    a.insertNewSymbol('%', "-.-.-.-.");

    a.displayAllSymbols();
    */

    cout << "Displaying all the symbols" << endl;
    parser.displayAllSymbols();

    return 0;
}

/*
    hey jude
    .... . -.--   .--- ..- -.. .
*/

