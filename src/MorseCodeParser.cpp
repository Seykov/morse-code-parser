#include <iostream>
#include "MorseCodeParser.h"

using namespace std;

MorseCodeParser::MorseCodeParser(){
    Alphabet baseSymbols;
    morseToEnglish = baseSymbols.getEnglishAlphabetAndNumbers();
    allowedMorseSymbols = baseSymbols.getAllowedMorseSymbols();
}

char MorseCodeParser::getSymbol(string _morse){

    if(morseToEnglish.count(_morse)){
        return morseToEnglish[_morse];
    } else {
        cerr << "Invalid morse sequence! " << endl;
        return '\n';
    }
}

void MorseCodeParser::insertNewSymbol(char _symbol, string _morse){

    if(validateMorseSequence(_morse) == false){
        return;
    }

    if(validateInsertionOfNewSymbol(_symbol, _morse) == false){
        return;
    }

    morseToEnglish.insert({_morse, _symbol});
    cout << "Symbol successfully added!" << endl;
}

void MorseCodeParser::printMorsePair(map<string, char>::iterator iter){
    string morse = iter -> first;
    char symbol = iter -> second;

    cout << symbol << " : " << morse << endl;
}

void MorseCodeParser::displayAllSymbols(){
    map<string, char>::iterator iter = morseToEnglish.begin();

    while (iter != morseToEnglish.end()){
        printMorsePair(iter);
        iter++;
    }
}

bool checkIfIndexInBoundaries(int _idx, int _arraySize){
    return _idx < _arraySize;
}

string trimRightSide(string _sequence){
    int len = _sequence.length();

    int finishingSpaces = 0;
    for(int i = len - 1; i >= 0; i--){
        if(_sequence[i] == ' '){
            finishingSpaces += 1;
        } else{
            break;
        }
    }

    _sequence = _sequence.substr(0, len - finishingSpaces);
    return _sequence;
}

string MorseCodeParser::parseMorseSequence(string _sequence){

    _sequence = trimRightSide(_sequence);

    if(validateMorseSequence(_sequence) == false){
        return "\n";
    }

    int len = _sequence.length();
    string result = "";
    string currentMorse = "";
    for(int i = 0; i < len; i++){
        if(_sequence[i] != ' '){
            currentMorse += _sequence[i];
            if(i == len - 1){
                result += getSymbol(currentMorse);
            }
        } else {
            if(morseToEnglish.count(currentMorse)){
                result += getSymbol(currentMorse);
                currentMorse = "";
            } else {
                cerr << "Invalid Morse sequence ending at position " << i + 1 << endl;
                return "\n";
            }

            if(_sequence[i + 1] == ' ' && _sequence[i + 2] == ' '){
                result += ' ';
                i += 2;
            }
        }
    }
    return result;
}

bool MorseCodeParser::validateMorseSequence(string _sequence){
    int len = _sequence.length();
    for(int i = 0; i < len ; i++){
        if(allowedMorseSymbols.find(_sequence[i]) == allowedMorseSymbols.end()){
            cerr << "Morse sequences can only include . (dots), - (dashes) and intervals!" << endl;
            return false;
        }
        if(_sequence[i] == ' '){
            if(_sequence[i + 1] == ' '){
                if(checkIfIndexInBoundaries(i + 2, len)){
                    if(_sequence[i + 2] != ' '){
                        cerr << "Intervals must be either 1 (for letter separator) or 3(for words separator)!" << endl;
                        return false;
                    }
                    else if(checkIfIndexInBoundaries(i + 3, len)){
                        if(_sequence[i + 3] == ' '){
                            cerr << "Intervals must be either 1 (for letter separator) or 3(for words separator)!" << endl;
                            return false;
                        }
                    }
                }
                if(checkIfIndexInBoundaries(i + 2, len)){
                    i += 2;
                }
            }
        }

    }

    return true;
}

bool MorseCodeParser::validateInsertionOfNewSymbol(char _symbol, string _morse){

    if(morseToEnglish.count(_morse)){
        cerr << "The morse sequence " << _morse <<
                " already exists and corresponds to the symbol " << morseToEnglish[_morse] << endl;
        return false;
    }

    map<string, char>::iterator iter = morseToEnglish.begin();

    while(iter != morseToEnglish.end()){
        char currentSymbol = iter -> second;
        if(currentSymbol == _symbol){
            cerr << "The symbol" << _symbol <<
                    " already has a corresponding morse code to it: " << iter -> first << endl;
            return false;
        }
        iter++;
    }
    return true;
}

