#include <iostream>
#include <fstream>
#include "Alphabet.h"

using namespace std;

Alphabet::Alphabet(){
    createEnglishAlphabetWithNumbers();
    defineAllowedMorseSymbols();
}

map<string, char> Alphabet::getEnglishAlphabetAndNumbers()const{
    return this->morseToEnglish;
}

set<char> Alphabet::getAllowedMorseSymbols()const{
    return this->allowedSymbols;
}

void Alphabet::createEnglishAlphabetWithNumbers(){

    ifstream file("alphabet.csv");
    string line;
    while(getline(file, line)){
        int len = line.length();
        char symbol = line[len - 1]; //last symbol in the line is the letter / digit
        string morse = line.substr(0, len - 2); //last two symbols in the line are comma and a letter / digit
        morseToEnglish.insert({morse, symbol});
    }
}

void Alphabet::defineAllowedMorseSymbols(){
    allowedSymbols.insert('.');
    allowedSymbols.insert('-');
    allowedSymbols.insert(' ');
}
